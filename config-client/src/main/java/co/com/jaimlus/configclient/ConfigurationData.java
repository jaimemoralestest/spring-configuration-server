package co.com.jaimlus.configclient;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("limites")
public class ConfigurationData {
	
	private int minimum;
    private int maximum;
    
    public int getMinimum() {
    	return this.minimum;
    }
    
    public int getMaximum() {
    	return this.maximum;
    }
    
}

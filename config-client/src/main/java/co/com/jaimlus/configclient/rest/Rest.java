package co.com.jaimlus.configclient.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.jaimlus.configclient.ConfigurationData;
import co.com.jaimlus.configclient.bean.BeanConfiguration;

@RestController
public class Rest {
	
	@Autowired
	private ConfigurationData configuration;
	
	@GetMapping("/limites")
	public BeanConfiguration getConfiguracion() {
		return new BeanConfiguration(configuration.getMinimum(), configuration.getMaximum());
	}
}

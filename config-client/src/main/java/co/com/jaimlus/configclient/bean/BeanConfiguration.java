package co.com.jaimlus.configclient.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BeanConfiguration {
	
	private int  minResultados;
	private int maxResultados;
	
	public  BeanConfiguration(int minResultado,int maxResultado)
	{
		this.minResultados=minResultado;
		this.maxResultados=maxResultado;
	}
}
